# git 사용하기

https://git-scm.com/book/ko/v2/Git의-기초-Git-Alias

https://git-scm.com/book/ko/v2

> 발표하는 모든 내용은 여기에 전부 설명 되어 있습니다.

## git과 svn 차이

- 각 파일의 변화를 시간순으로 관리하면서 파일들의 집합을 관리하지 않고 스냅샷으로 취급 관리
  - 브랜치와 태그 모두 하나의 디렉토리에서 이루어진다.
- 중앙집중식 버전 관리 vs 분산 버전 관리 시스템
  - 모든 history가 local에 존재한다.
  - 거의 모든 명령은 네트워크를 거칠 필요가 없다.

---

## 시작하기

### git 설치

https://git-scm.com/book/ko/v2/시작하기-Git-설치

OS에 맞게 설치

---

### git 저장소 만들기

init, clone

#### 1. init

아직 버전관리를 하지 않는 로컬 디렉토리 하나를 선택해서 Git 저장소를 적용하는 방법

root 디렉토리에서 `$ git init`

`.git` 디렉토리가 생성되는 걸 확인할 수 있다.

---

#### 2. clone

다른 어딘가에서 Git 저장소를 Clone 하는 방법

`$ git clone https://github.com/libgit2/libgit2`

- remote repository의 git 데이타를 포함하는 프로젝트를 가져온다.

- Git은 다양한 프로토콜을 지원한다.
  `https://` 프로토콜, `git://` 를 사용할 수도 있고 `user@server:path/to/repo.git` 처럼 SSH 프로토콜을 사용할 수도 있다.

---

### status

<p align="middle"><img src="https://git-scm.com/book/en/v2/images/areas.png" width="50%"/></p>

git은 `Committed`, `Modified`, `Staged` 세가지 상태를 갖는다.

- `Committed`란 데이터가 로컬 데이터베이스에 안전하게 저장됐다는 것을 의미한다.

- `Modified`는 수정한 파일을 아직 로컬 데이터베이스에 커밋하지 않은 것을 말한다.

- `Staged`란 현재 수정한 파일을 곧 커밋할 것이라고 표시한 상태를 의미한다.

---

<p align="middle"><img src="https://git-scm.com/book/en/v2/images/lifecycle.png" width="100%"/></p>

---

`$ git status`

- 디렉토리 내 file들의 상태를 확인

`$ git add README.md`

- 파일을 새로 추적하기
- 파일을 새로 추적할 때도 사용하고 수정한 파일을 Staged 상태로 만들 때도 사용
- deleted된 파일도 staged 상태로 만들기 가능 (git 2.0.0부터)

`$ git rm README.md`

- 파일을 삭제 하기
- 디렉토리의 파일도 삭제 된다.
- `$ git rm --cached README.md`를 이용하면 하드디스크에 있는 파일은 그대로 두고 Git만 추적하지 않게 한다.

---

### .gitignore

Project에 원하지 않는 파일들을(컴파일 된 파일, .gradle, .idea 등등) git에서 제외시키기 위해 필요한 파일

- .gitignore의 위치에서 하위로 효과가 전파된다.

```bash
# 확장자가 .class인 파일은 무시합니다.
*.class

# .class 파일들은 모두 무시되지만, HelloWorld.class만큼은 무시하지 않습니다.
!HelloWorld.class

# 현재 디렉터리에 있는 /Test.java 파일은 무시되지만, subDir/Test.java 같이 특정 디렉터리 하위에 있는 Test.java는 무시되지 않습니다.
/Test.java

# target/ 디렉터리에 있는 모든 파일 무시합니다.
target/

# src/ 하위의 .java 파일은 무시되지만 /src/main/ 하위의 .java 파일은 무시되지 않습니다.
src/*.java

# src/ 하위에 존재하는 모든 디렉터리의 .txt 파일을 무시합니다.
src/**/*.txt
```

---

### commit

#### 변경사항 커밋하기

`$ git commit`

커밋메시지를 자세히 적어 주자!

인라인으로 커밋메시지 적을 수도 있다.

`$ git commit -m "커밋메시지"`

---

### 되돌리기

#### 커밋 재작성

`--amend`

- 이전 커밋에서 뭔가를 빠뜨려서 커밋을 재작성하고 싶을때 사용
  - 파일 누락, 오타 수정등

#### 파일 상태를 Unstage로 변경하기

`$ git reset HEAD <file>...`

- 따로 커밋을 해야하는 파일을 모두 add 해버렸을때 사용한다.
- 단순히 staged -> unstaged 로 이동할 뿐이다.

#### Modified 파일 되돌리기

`$ git checkout -- <file>...`

- 수정된 파일을 이전 커밋 상태로 되돌리기
- 변경된 수정사항을 되돌릴 수 없다.

---

### log

#### 커밋 히스토리 조회하기

`$ git log`

--patch 와 갯수를 지정한경우

`$ git log -p -2` 변경사항과 최근 2개의 히스토리를 조회

`$ git log --graph` 그래프와 함께 히스토리 조회 (branch가 있을경우 유용함)

`$ git log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --all` 엄청 예쁘게

---

### remote

`git remote` 리모트로 등록된 이름을 보여준다.

`$ git remote -v` 이름과 url을 모두 볼 수 있다.

`$ git remote show <리모트 저장소 이름>` 특정 리모트 정보 상세보기

리모트 저장소 추가하기
`$ git remote add <단축이름> <url>`

리모트 저장소 이름 바꾸기
`$ git remote rename <지금이름> <바꿀이름>`

리모트 저장소 삭제
`$ git remote remove <단축이름>`

---

### pull (fetch + merge)

#### 리모트 서버로부터 저장소 정보 동기화

`$ git fetch`

`$ git pull` = `git fetch` + `git merge`

브랜치에서 작업중 해당 브랜치의 HEAD가 많이 진행되었고 업데이트 하고 싶다면 merge보단 rebase를 사용하자!

`$ git pull --rebase <리모트 저장소 이름> <브랜치 이름>`

**!주의**

> 한번 공개된 히스토리는 함부로 바꿔서는 안된다. `rebase`는 자신의 작업그래프에서만 사용 (커밋 히스토리 정리)

> 마지막 푸시전 사용하면 유용!

서로 다른 히스토리의 프로젝트를 merge하고 싶을때

`$ git pull origin master --allow-unrelated-histories`

**!주의**

> 머지 실패가 빈번히 일어날 수 있음

---

### push

- 이 명령은 Clone 한 리모트 저장소에 쓰기 권한이 있고,
- 먼저 다른 사람이 작업한 것을 가져와서 Merge 한 후에 Push 할 수 있다

`$ git push <리모트 저장소 이름> <브랜치 이름>`

강제로 푸시하기 (rebase commit history를 조작했을 경우 새로운 hash값이 생성 된다.)

`$ git push -f <remote-repository> <branch-name>`

> 단 재앙이 일어나기 쉽다.

---

### branch

- 코드를 통째로 복사하고 원래 코드와 상관없이 독립적으로 개발을 진행할 수 있게 해준다.
- 작업적 브랜치를 만들고 나중에 Merge하는 방법을 권장

#### 새 브랜치 생성하기

`$ git branch testing`

#### 브랜치 이동하기

`$ git checkout testing`

#### 브랜치 생성하고 이동하기

`$ git checkout -b testing`

### 브랜치 삭제하기

`$ git branch -d hotfix`

---

### merge

합칠 브랜치에서 합쳐질 브랜치를 merge한다.

`(master)$ git merge iss53`

#### fast-forward

A 브랜치에서 다른 B 브랜치를 Merge 할 때 B 브랜치가 A 브랜치 이후의 커밋을 가리키고 있으면 그저 A 브랜치가 B 브랜치와 동일한 커밋을 가리키도록 이동시킬 뿐이다.

#### no fast-forward

`(master)$ git merge --no-ff iss53`

fast forwad merge가 가능하더라도 merge commit을 만들어 merge한다.

#### 3-way Merge

공통조상으로 올라가 3-way merge를 한다.

---

#### 충돌

같은 부분을 동시에 수정하고 merge할경우

```bash
$ git merge iss53
Auto-merging index.html
CONFLICT (content): Merge conflict in index.html
Automatic merge failed; fix conflicts and then commit the result.
```

merge 실패 파일을 확인한다.

```bash
$ git status
On branch master
You have unmerged paths.
  (fix conflicts and run "git commit")

Unmerged paths:
  (use "git add <file>..." to mark resolution)

    both modified:      index.html

no changes added to commit (use "git add" and/or "git commit -a")
```

---

```
 <<<<<<< HEAD:index.html
 <div id="footer">contact : email.support@github.com</div>
 =======
 <div id="footer">
  please contact us at support@github.com
 </div>
 >>>>>>> iss53:index.html
```

======= 위쪽의 내용은 HEAD 버전(merge 명령을 실행할 때 작업하던 master 브랜치)의 내용이고 아래쪽은 iss53 브랜치의 내용

<<<<<<<, =======, >>>>>>>`가 포함된 행을 삭제. 이렇게 충돌한 부분을 해결하고 `git add 명령으로 다시 Git에 저장

---

### tag

보통 릴리즈할 때 사용한다(v1.0, 등등)

`git tag`

`$ git tag -a <tag 이름> -m <메시지>` 태그 추가

`$ git show <tag 이름>` 태그 보기

`$ git tag -a <tag name> <checksum>` 예전 커밋에 태그추가

---

태그 공유하기

`$ git push <리모트 이름> <tag 이름>`

`$ git push <리모트 이름> --tags` 리모트 저장소에 없는 태그 모두 전송

`$ git push <리모트 이름> :<tag 이름>` 리모트 저장소에 있는 태그 삭제

태그를 Checkout 하기

- 태그를 체크아웃하면 “detached HEAD”상태가 된다.

- 이 상태에서 커밋이 쌓인다면 이전 커밋으로 쉽게 도달할 수 없다. - detached HEAD를 새로운 브랜치로 만들어 이용하는게 좋다.

  `$ git checkout -b version2 v2.0.0`

---

### 커밋 히스토리 정리하기

push전 커밋 히스토리를 정리하는게 좋다. publish된 커밋 히스토리는 변경하지 않는게 가장 바람직하기 때문.

#### squash

여러개의 커밋 히스토리를 하나로 합쳐주는 방법

- `$ git rebase -i HEAD~3` 3개의 커밋을 interactive rebase한다.
- pick 부분을 squash로 변경
- 새로운 커밋메시지 작성

#### rebase

푸시전 작업 브랜치 업데이트 (협업시 내가 시작한 브랜치에서 업데이트가 일어났을 경우)

`$ git pull --rebase upstream feature/newfeature`

이렇게 잘 정리하면 linear한 커밋 히스토리를 관리할 수 있다.

---

### merge vs rebase

#### merge

<p align="middle"><img src="https://wac-cdn.atlassian.com/dam/jcr:e229fef6-2c2f-4a4f-b270-e1e1baa94055/02.svg?cdnVersion=1316" width="100%"/></p>

---

#### rebase

<p align="middle"><img src="https://wac-cdn.atlassian.com/dam/jcr:5b153a22-38be-40d0-aec8-5f2fffc771e5/03.svg?cdnVersion=1316" width="100%"/></p>

---

### reset vs checkout vs revert

- Revert로 되돌리면 commit 이력을 유지한 채로 파일내용을 되돌림
  - A-B-C-D -> A-B-C-D-B'-E
- Reset으로 되돌릴 경우 commit 이력을 삭제한 채 파일내용을 되돌림
  - A-B-C-D -> A-B-(~~C-D~~)-E
  - 공동으로 작업할경우 HEAD가 꼬일 수 있으므로 한번 publish된 커밋이력을 되돌릴경우 주의한다.
- Checkout을 사용하면 단순히 HEAD만 해당 커밋으로 이동한다.
  - A-B-C-D -> A-B-E (C-D)
  - checkout 한 이후 헤드가 진행되면 그 이후의 커밋들(C, D)은 고아가 된다.
  - “detached HEAD”상태가 되기 때문에 브랜치를 생성해서 작업하는게 좋다.

---

#### reseting

<p align="middle"><img src="https://wac-cdn.atlassian.com/dam/jcr:4c7d368e-6e40-4f82-a315-1ed11316cf8b/02-updated.png?cdnVersion=1316" width="50%"/></p>

---

#### checking out

<p align="middle"><img src="https://wac-cdn.atlassian.com/dam/jcr:607f1b83-ee7d-494a-b7e2-338d810059fb/04-updated.png?cdnVersion=1316" width="50%"/></p>

---

#### reverting

<p align="middle"><img src="https://wac-cdn.atlassian.com/dam/jcr:73d36b14-72a7-4e96-a5bf-b86629d2deeb/06.svg?cdnVersion=1316" width="50%"/></p>

---

# gitlab

설치형 git 원격저장소

- Issue tracker 제공
- git 원격저장소 제공
- API 제공
- Team, Group 기능 제공

## 이니시스 gitlab

http://192.168.156.44

---

## 프로젝트 생성하기

---

## 이슈 등록하기

- close를 어렵게 생각하지 말자 (다시 open하면 된다.)

---

## mr 생성하기

- forking한 저장소에서 -> forked된 저장소로 mr 생성
- 브랜치1에서 -> 브랜치 2로 mr 생성

---

# git-flow

> https://nvie.com/posts/a-successful-git-branching-model/

항상 유지되는 메인 브랜치들(master, develop)
일정 기간 동안만 유지되는 보조 브랜치들(feature, release, hotfix)

- master : 제품으로 출시될 수 있는 브랜치
- develop : 다음 출시 버전을 개발하는 브랜치
- feature : 기능을 개발하는 브랜치
- release : 이번 출시 버전을 준비하는 브랜치
- hotfix : 출시 버전에서 발생한 버그를 수정 하는 브랜치

---

<p align="middle"><img src="https://woowabros.github.io/img/2017-10-30/git-flow_overall_graph.png" width="50%"/></p>

- feature 완료후 develop으로 merge된다.
- hoxfix 완료후 master와 develop으로 merge된다.
- release 완료후 master와 develop으로 merge된다. tag가 생성된다.

---

- Develop브랜치
  develop브랜치에는 상시로 버그를 수정한 커밋들이 추가되게 됩니다.
  새로운 기능을 추가하는 경우 develope 브랜치에서 시작하는 feature브랜치를 생성합니다

- Feature브랜치
  feature브랜치는 기능 추가 작업이 완료되었다면, develop 브랜치로 merge 합니다.
  새로운 기능을 추가하는 경우 develope 브랜치에서 시작하는 feature브랜치를 생성합니다

---

- release 브랜치
  develop브랜치에 이번 버전에 포함하는 모든 기능이 merge되었다면 , QA를 위해 develop브랜치에서 release브랜치를 생성합니다.
  QA를 무사히 통과하게되면 relase브랜치를 master와 develop브랜치로 merge 합니다.
  QA를 진행하면서 발생한 버그들은 모두 release브랜치에 수정되게 됩니다

- 출시후 master 브랜치에서 해당 커밋에 버전 태그를 추가합니다.

---

- 개발자는 develop과 master브랜치에 푸시 할 일이 없음
- 한번 푸시한 커밋히스토리를 수정하려면 다른사람들과 히스토리를 꼭 맞춰야함
- 관리자는 master, develop으로 머지, 브랜치 삭제와 태그 추가를 푸시 해주어야함

---

# 실습

~~http://192.168.156.44/welcome/git-flow-test~~

~~forking하기~~

~~`http://192.168.156.44/{user id}/git-flow-test`~~

https://gitlab.com/kimdugong/git-flow-test

```bash
$ git clone http://192.168.156.44/{user id}/git-flow-test
$ cd git-flow-test
$(master) git checkout develop
$(develop) git flow feature start test
$(test) touch test
$(test) git status
$(test) git add test
$(test) git commit -m "test 파일 추가"
$(test) git flow publish
$(test) git flow feature pull origin test
$(test) git flow publish
```

---

- 머지리퀘스트 작성
- 코드리뷰
- 머지

```bash
$(test) git flow feature finish
$(develop) git pull -r origin develop
$(develop) git log --graph
```

---

# 작업흐름

0. 소스코드 최신화 or clone
1. git flow 초기화
2. 브랜치 생성
3. 소스코드 pull (누군가의 작업물이 필요할경우)
4. 소스코드 push (작업이 길어질 경우)
5. 작업 완료
6. 소스코드 pull (소스코드 최신화)
7. 소스코드 push (feature, hoxfix)
8. merge request
9. code review
10. merge into develop branch
11. merge

---

`$ git clone -o upstream http://192.168.156.44/welcome/git-flow-test.git`

로컬과 upstream에 브랜치 생성하고 체크아웃

1. `$ git checkout -b bfm-100_login_layout --track upstream/feature-user`
2. 소스수정
